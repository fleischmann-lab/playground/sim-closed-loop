# Simulate closed loop experiment

## Description

This repository is a playground to simulate closed loop experiment, i.e. a stim A with "high" measure of behavior and a stim B randomly in the experiment but with similar rates as A.

Currently this is via Python. But the logic (i.e. Poisson process with refractory period) can be adapted to Arduino/Teensy, although it needs checking that the random number generation works properly on such microcontrollers.

## Download

To run the notebook, please clone first with:

```bash
git clone https://gitlab.com/fleischmann-lab/playground/sim-closed-loop.git
```

## Requirements

The requirements can be installed with:

```bash
pip install -r requirements.txt
```

Then use `jupyter` however you want.

## Data

This does require a demo Teensy data file to simulate the `flow` inputs. Do the following to get it:

```bash
wget -P data https://gin.g-node.org/fleischmann-lab/calimag-testdata/raw/orig/data/v2022_05-2p-Max/20220531_ME02.txt 
```
