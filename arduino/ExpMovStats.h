#ifndef ExpMovStats_h
#define ExpMovStats_h
#include <Arduino.h>


// Exponential moving statistics
// Note: public attrs ~ for ease of use
class ExpMovStats {

  // Structs

  // Parameters
  struct Parameters {
    double        alpha;
    uint16_t      init_sample;
    bool          init_with_stats;
  };

  // States
  struct States {
    double        mean = 0;
    double        var = 0;
    double        std = 0;
  };

  // Auxiliary
  struct Auxiliary {
    bool          init_is_done = false;
  };


  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Constructor
    ExpMovStats(double alpha, uint16_t init_sample, bool init_with_stats);

    // Methods
    void step(double value, uint32_t sample);
};

#endif
