#include "FixedPeriodMicroscope.h"

FixedPeriodMicroscope::FixedPeriodMicroscope(
  double tau_trial,
  double tau_ITI,
  double tau_delay
) {
  // Constructor

  // Assign parameters
  params.tau_trial = tau_trial;
  params.tau_ITI = tau_ITI;
  params.tau_delay = tau_delay;

}


void FixedPeriodMicroscope::step(double current_time) {
  // Delay scope
  if (current_time < params.tau_delay) {
    return;
  }
  
  // Turn ON scope if not in a recent trial
  if (
    aux.current_onset_time < 0
    // && !states.is_on // not necessary?
  ) {
    states.output = true; // use this to turn scope ON
    states.is_on = true;
    states.trial += 1;
    aux.current_onset_time = current_time;
    return;
  }

  double time_from_onset = current_time - aux.current_onset_time;

  // During trial
  // Keep track of ON state during trial
  // but no output to scope
  if (time_from_onset < params.tau_trial) {
    states.output = false;
    states.is_on = true;
    return;
  }

  // During ITI
  // Keep track of OFF state of scope
  // no need to send output to scope to turn off
  if (time_from_onset < (params.tau_trial + params.tau_ITI)) {
    states.output = false;
    states.is_on = false;
    return;
  }
    
  // Exceeding past current trial + ITI
  // Reset onset time to allow new trial on next `step`
  aux.current_onset_time = -1;

}
