#ifndef Event_h
#define Event_h
#include <Arduino.h>


// Event
// Note: public attrs ~ for ease of use
// Note: it may be possible to have this as a superclass 
//        for `Poisson`, `PeakViaDev`, `ThresEvent`,
//        later to consider
// Note: this is currently not used
class Event{

  // Struct
  // Parameters
  struct Parameters {
    double        dt;
    double        tau_refrac;
    uint8_t       max_consec; // use 0 to suppress this condition
    double        init_time; // keep this small ~ 1-2 sec
  };

  // States
  struct States {
    bool          output = false;
    uint32_t      cum_pre_count = 0;
    unsigned int  cum_count = 0;
    unsigned int  consec_count = 0;
  };

  // Auxiliary
  struct Auxiliary {
    double        last_output_time = -1;
  };

  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Constructor
    Event(
      double dt,
      double tau_refrac,
      uint8_t max_consec,
      double init_time
    );

    // Methods
    void step(double value, uint32_t sample);
};

#endif
