#include "PeakViaDev.h"

PeakViaDev::PeakViaDev(
  double dt,
  double theta,
  double tau_delay,
  double tau_refrac,
  ExpMovStats* ems
) {
  // Constructor

  // Assign parameters
  params.dt = dt;
  params.theta = theta;
  params.tau_delay = tau_delay;
  params.tau_refrac = tau_refrac;

  // Assign pointers
  this->ems = ems;
}


void PeakViaDev::step(
  double value,
  uint32_t sample
) {
  // Update states
  double t = params.dt * (double) sample;

  // Check for threshold crossing from deviation
  aux.thres_cross = value > (
      ems->states.mean +
      (params.theta * ems->states.std)
  );

  // Sustained crossing to support delaying peak
  // also, minimum crossing time to consider peak
  // e.g. to account for moving statistics delay
  // and to avoid large brief artifacts
  aux.sustained_cross = (
    ((double) aux.thres_cross) * (
    aux.sustained_cross + params.dt
  ));

  bool delay_is_met = aux.sustained_cross > params.tau_delay;

  // Refractory period of peak detection,
  // i.e. min distance between peak
  bool refrac_is_met = (
    aux.last_peak_time < 0 ||
    (t - aux.last_peak_time > params.tau_refrac)
  );

  // Peak
  states.peak = delay_is_met && refrac_is_met;

  // Save
  if (states.peak) {
    aux.last_peak_time = t;
  }
}
