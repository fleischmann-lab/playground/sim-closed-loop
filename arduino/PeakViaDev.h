#ifndef PeakViaDev_h
#define PeakViaDev_h
#include <Arduino.h>
#include "ExpMovStats.h"


// Peak detection via deviation
// Note: public attrs ~ for ease of use
class PeakViaDev {

  // Struct
  // Parameters
  struct Parameters {
    double        dt;
    double        theta;
    double        tau_delay;
    double        tau_refrac;
  };

  // States
  struct States {
    bool          peak = false;
  };

  // Auxiliary
  struct Auxiliary {
    double        last_peak_time = -1;
    bool          thres_cross = false;
    double        sustained_cross = 0;
  };


  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Pointers
    ExpMovStats* ems;


    // Constructor
    PeakViaDev(
      double dt,
      double theta,
      double tau_delay,
      double tau_refrac,
      ExpMovStats* ems
    );

    // Methods
    void step(double value, uint32_t sample);
};

#endif
