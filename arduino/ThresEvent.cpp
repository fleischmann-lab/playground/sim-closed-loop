#include "ThresEvent.h"


ThresEvent::ThresEvent(
  double dt,
  double thres,
  double tau_refrac,
  double tau_pre_refrac,
  uint8_t max_consec,
  double init_time,
  bool is_independent,
  double* value
) {
  // Constructor

  // Assign parameters
  params.dt = dt;
  params.thres = thres;
  params.tau_refrac = tau_refrac;
  params.tau_pre_refrac = tau_pre_refrac;
  params.max_consec = max_consec;
  params.init_time = init_time;
  params.is_independent = is_independent;

  // Assign pointers
  this->value = value;
}


void ThresEvent::step(
  uint32_t sample
) {
  // Update states
  double t = params.dt * (double) sample;

  // Initialization
  if (t < params.init_time) { // do nothing
    states.output = false;
    return;
  }

  // Compare with threshold
  bool thres_is_met = *value > params.thres;

  // Additional conditions
  // Refractory of real event (may be duplicate but OK)
  bool refrac_is_met = (
    aux.last_output_time < 0 ||
    (t - aux.last_output_time > params.tau_refrac)
  );

  // Refractory of threshold crossing events
  bool pre_refrac_is_met = (
    aux.last_thres_cross_time < 0 ||
    (t - aux.last_thres_cross_time > params.tau_pre_refrac)
  );

  // Max consecutively similar events
  bool consec_is_met = (
    params.max_consec == 0 ||
    (states.consec_count < params.max_consec)
  );

  // Update pre-counts, i.e. only thres cross, no refractory
  if (
    thres_is_met &&
    pre_refrac_is_met
  ) {
    states.cum_pre_count += 1;
    aux.last_thres_cross_time = t;
  }

  // Determine to output
  if (
    thres_is_met &&
    refrac_is_met &&
    consec_is_met
  ) {
    states.output = true;

    // update actual cumulative count
    states.cum_count += 1;
    states.consec_count += 1;

    // note: if indepdendent, do this
    // other wise, update outside when other events can influence
    if (params.is_independent) {
      aux.last_output_time = t;
    }

  } else {
    states.output = false;
  }
}

void ThresEvent::reset_consec() {
  states.consec_count = 0;
}
