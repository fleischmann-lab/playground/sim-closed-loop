#include "TriggeredBudgetedMicroscope.h"


TriggeredBudgetedMicroscope::TriggeredBudgetedMicroscope(
  double dt,
  double tau_trial,
  double tau_min_ITI,
  double tau_delay,
  double tau_max_budget,
  double tau_pre_offset
) {
  // Constructor

  // Assign parameters
  params.dt = dt;
  params.tau_trial = tau_trial;
  params.tau_min_ITI = tau_min_ITI;
  params.tau_delay = tau_delay;
  params.tau_max_budget = tau_max_budget;
  params.tau_pre_offset = tau_pre_offset;
}


void TriggeredBudgetedMicroscope::step(bool event_input, double current_time) {

  // Update budget tracker  
  update_budget();

  // Do nothing during delay
  if (current_time < params.tau_delay) {
    return;
  }

  // Determine some conditions

  bool not_in_current_trial = aux.current_onset_time < 0;

  bool promise_is_met = (
    aux.promise_onset_time > 0 &&
    current_time > aux.promise_onset_time
  );

  bool budget_is_met = (states.budget < params.tau_max_budget);

  bool scope_is_ready =  (
    (event_input || promise_is_met) &&
    budget_is_met
  );

  // bool scope_is_off = !states.is_on; // potentially redudant

  // Do nothing if not in current trial but scope is not ready 
  if (not_in_current_trial && !scope_is_ready) {
    return;
  }
  
  // Trigger scope if ready
  if (not_in_current_trial && scope_is_ready) {
    states.output = true; // use this to turn scope ON
    states.is_on = true;
    states.trial += 1;
    aux.current_onset_time = current_time;
    aux.promise_onset_time = -1;
    return;
  }

  // Anything onwards from here means in current trial

  double time_from_onset = current_time - aux.current_onset_time;

  // During trial, before pre-offset period
  // Keep track of ON state during trial
  // No output to scope
  // Events do not matter here
  if (time_from_onset < params.tau_trial - params.tau_pre_offset) {
    states.output = false;
    states.is_on = true;
    return;
  }

  double potential_promised_time = (
    aux.current_onset_time +
    params.tau_trial +
    params.tau_min_ITI
  );

  // During pre-offset period
  // Similar to above, but event can induce a promised time
  if (time_from_onset < params.tau_trial) {
    states.output = false;
    states.is_on = true;

    if (event_input) {
      aux.promise_onset_time = potential_promised_time;
    }
    return;
  }

  // During min ITI
  // Turn off state `is_on`
  if (time_from_onset < (params.tau_trial + params.tau_min_ITI)) {
    states.output = false;
    states.is_on = false;

    if (event_input) {
      aux.promise_onset_time = potential_promised_time;
    }
    return;
  }

  // Exceeding past current trial + ITI
  // Reset onset time to allow new trial on next `step`
  aux.current_onset_time = -1;
  
}


void TriggeredBudgetedMicroscope::update_budget() {
  // Currently this is a very naive implementation
  // to be used as placeholder logic
  // TODO: because of the rectangular function of scope
  //        this can be re-implemented to capture ON time 
  //        of the scope for every minute using Queue FIFO
  //        see below for draft logic and prerequsites

  states.budget += (
    params.dt * (
      2.0 * ((double) states.is_on) 
      - 1.0
    )
  );

  /* 
  Draft logic of actual estimation of budget per a given interval

  Needs also:
  - params.tau_budget_interval
  - aux.trigger_onset_queue ~ FIFO queue size `int(tau_budget_interval / (tau_trial + tau_min_ITI))`
  - some check to make sure `params.tau_budget_interval > params.tau_max_budget`

  This depends on Queue package or custom implementation TBD that has methods to:
  - get number of records, via `.num()` (or the like)
  - peek the latest record, via `peek_latest()`
  - peek the oldest record, via `peek_oldest()`

  Pseudo-algo:
    // Push the most current onset time
    if (states.output) {
      aux.trigger_onset_queue.push(aux.current_onset_time);
    }

    // Pop the oldest item if out of range
    if (
      aux.trigger_onset_queue.peek_oldest()
      < (
        current_time 
        - params.tau_budget_interval
        - params.tau_trial
      )
    ) {
      aux.trigger_onset_queue.pop();
    }

    int num_total_records = aux.trigger_onset_queue.num();

    // Budget in interval maximum possible
    states.budget = (double) num_total_records * params.tau_trial;

    // Subtract oldest ON duration outside of budget interval
    double oldest_outside_remainder = (
      current_time
      - params.tau_budget_interval
      - aux.trigger_onset_queue.peek_oldest()
    );

    if (oldest_outside_remainder > 0) {
      states.budget -= oldest_outside_remainder;
    }

    // Subtract latest ON duration not yet reached
    double latest_outside_remainder = (
      aux.trigger_onset_queue.peek_latest()
      + params.tau_trial
      - current_time
    );

    if (latest_outside_remainder > 0) {
      states.budget -= latest_outside_remainder;
    }
    
  */
}