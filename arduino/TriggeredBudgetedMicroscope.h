#ifndef TriggeredBudgetedMicroscope_h
#define TriggeredBudgetedMicroscope_h
#include <Arduino.h>


// Event-triggered Micropscope with Budget event generation
// Note: public attrs ~ for ease of use
class TriggeredBudgetedMicroscope {

  // Struct
  // Parameters
  struct Parameters {
    double        dt;
    double        tau_trial;
    double        tau_min_ITI;
    double        tau_delay;
    double        tau_max_budget;
    double        tau_pre_offset;
  };

  // States
  struct States {
    double        budget = 0;
    bool          output = false;
    bool          is_on = false;
    unsigned int  trial = 0;
  };

  // Auxiliary
  struct Auxiliary {
    double        current_onset_time = -1;
    double        promise_onset_time = -1;
  };

  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Constructor
    TriggeredBudgetedMicroscope(
      double dt,
      double tau_trial,
      double tau_min_ITI,
      double tau_delay,
      double tau_max_budget,
      double tau_pre_offset
    );

    // Methods

    void step(bool event_input, double current_time);

    void update_budget();
    
};

#endif
