#ifndef ThresEvent_h
#define ThresEvent_h
#include <Arduino.h>


// ThresEvent
// Note: public attrs ~ for ease of use
class ThresEvent{

  // Struct
  // Parameters
  struct Parameters {
    double        dt;
    double        thres;
    double        tau_refrac;
    double        tau_pre_refrac;
    uint8_t       max_consec; // use 0 to suppress this condition
    double        init_time; // keep this small ~ 1-2 sec
    bool          is_independent; // whether event count/ is independent from other events
  };

  // States
  struct States {
    bool          output = false;
    uint32_t      cum_pre_count = 0;
    unsigned int  cum_count = 0;
    unsigned int  consec_count = 0;
  };

  // Auxiliary
  struct Auxiliary {
    double        last_output_time = -1;
    double        last_thres_cross_time = -1;
  };

  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Pointers
    double* value;

    // Constructor
    ThresEvent(
      double dt,
      double thres,
      double tau_refrac,
      double tau_pre_refrac,
      uint8_t max_consec,
      double init_time,
      bool is_independent,
      double* value
    );

    // Methods
    void step(uint32_t sample);

    void reset_consec();
};

#endif
