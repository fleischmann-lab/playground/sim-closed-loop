#include "Poisson.h"


// static double (*Poisson::generate_random_func)();    
static double Poisson::generate_random_func() {
  return (double) random(1000000L);
};

Poisson::Poisson(
  double dt,
  double tau_refrac,
  double init_rate,
  uint8_t max_consec,
  double init_time,
  bool is_independent
) {
  // Constructor

  // Assign parameters
  params.dt = dt;
  params.tau_refrac = tau_refrac;
  params.init_rate = init_rate;
  params.max_consec = max_consec;
  params.init_time = init_time;
  params.is_independent = is_independent; 

}


void Poisson::set_target_rate(
  double target_rate
) {
  states.target_rate = target_rate;
}


void Poisson::step(
  uint32_t sample
) {
  // Update states
  double t = params.dt * (double) sample;

  // Initialization
  if (t < params.init_time) {
    states.r = params.init_rate;
    states.target_rate = params.init_rate;
    states.output = false;
    return;
  }

  /*
  Compare with random number to pre-determine
  whether to output event probabilistically
  The generator needs to have max 1E6 because
  units of `r` = mHz and `dt` = ms
  */
  double rand_num = generate_random_func();
  bool thres_is_met = (states.r * params.dt) > rand_num;

  // Additional conditions
  // Refractory (may be duplicate but OK)
  bool refrac_is_met = (
    aux.last_output_time < 0 ||
    (t - aux.last_output_time > params.tau_refrac)
  );

  // Max consecutively similar events
  bool consec_is_met = (
    params.max_consec == 0 ||
    (states.consec_count < params.max_consec)
  );

  // Update pre-counts, i.e. only thres cross, no refractory
  if (thres_is_met) {
    states.cum_pre_count += 1;
  }

  // Determine to output
  if (
    thres_is_met &&
    refrac_is_met &&
    consec_is_met
  ) {
    states.output = true;

    // resset internal -> 0 to accommodate refractory
    states.r = 0;

    // update actual cumulative counts
    states.cum_count += 1;
    states.consec_count += 1;

    // note: if indepdendent, do this
    // other wise, update outside when other events can influence
    if (params.is_independent) {
      aux.last_output_time = t;
    }

  } else {
    states.output = false;

    // Update internal states to satisfy target
    // with refractory consideration
    states.r += (
      params.dt *
      (states.target_rate - states.r)
      / params.tau_refrac
    );
  }
}

void Poisson::reset_consec() {
  states.consec_count = 0;
}

