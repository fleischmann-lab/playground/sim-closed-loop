#ifndef FixedPeriodMicroscope_h
#define FixedPeriodMicroscope_h
#include <Arduino.h>


// Microscope trigger with a fixed period
// Note: public attrs ~ for ease of use
class FixedPeriodMicroscope {

  // Struct
  // Parameters
  struct Parameters {
    double        tau_trial;
    double        tau_ITI;
    double        tau_delay;
  };

  // States
  struct States {
    bool          output = false;
    bool          is_on = false;
    unsigned int  trial = 0;
  };

  // Auxiliary
  struct Auxiliary {
    double        current_onset_time = -1;
  };

  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Constructor
    FixedPeriodMicroscope(
      double tau_trial,
      double tau_ITI,
      double tau_delay
    );

    // Methods
    void step(double current_time);
    
};

#endif
