#include "ExpMovStats.h"

ExpMovStats::ExpMovStats(
  double alpha,
  uint16_t init_sample,
  bool init_with_stats
) {
  // Constructor

  // Assign parameters
  params.alpha = alpha;
  params.init_sample = init_sample;
  params.init_with_stats = init_with_stats;
}


void ExpMovStats::step(
  double value,
  uint32_t sample
) {
  // Update states
  if (!aux.init_is_done) {
    if (sample < params.init_sample) {
      // during initialization
      if (params.init_with_stats) {
        states.mean += value;
        states.var += (value * value);
      }
    }
    else {
      // finalize initialization
      if (params.init_with_stats) {
        states.mean /= ((double) sample);
        states.var = states.var / ((double) sample) - (states.mean * states.mean);
        states.std = sqrt(states.var);
      }
      aux.init_is_done = true;
    }
  }
  else {
    // update with smoothing
    double delta = value - states.mean;
    states.mean += (params.alpha * delta);
    states.var = (1 - params.alpha) * (states.var + params.alpha * delta * delta);
    states.std = sqrt(states.var);
  }
}


