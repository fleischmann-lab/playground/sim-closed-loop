# Simulate with Wokwi

See: <https://wokwi.com/projects/394362002620148737>

Currently:

- [x] Sniff peak detection
- [x] Sniff rate calculation
- [x] Sniff rate threshold detection
- [x] Event A paired with sniff rate threshold crossing
- [x] Event B as random Poisson process with refractory period, somewhat rate-matched with A (sort of)
- [x] Event C also as random Poisson process occurring less frequently than B
- [x] Fixed period microscope trigger
- [ ] Event triggered microscope with budget
  - [x] Implemented a rough draft, still need to do a few more things
- [ ] Need to have a rough python script/notebook to read data and check conditions
- [ ] Misc: need to figure out how to print configurations and parameters in headers
- [ ] Misc: need to figure out better way to report data more programmatically

