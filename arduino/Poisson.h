#ifndef Poisson_h
#define Poisson_h
#include <Arduino.h>


// Poisson event generation
// Note: public attrs ~ for ease of use
class Poisson {

  // Struct
  // Parameters
  struct Parameters {
    double        dt;
    double        tau_refrac;
    double        init_rate;
    uint8_t       max_consec; // use 0 to suppress this condition
    double        init_time; // keep this small ~ 1-2 sec
    bool          is_independent; // whether event count/ is independent from other events
  };

  // States
  struct States {
    double        r = 0;
    double        target_rate = 0;
    bool          output = false;
    uint32_t      cum_pre_count = 0;
    unsigned int  cum_count = 0;
    unsigned int  consec_count = 0;
  };

  // Auxiliary
  struct Auxiliary {
    double        last_output_time = -1;
  };

  public:
    // Attributes
    Parameters params;
    States states;
    Auxiliary aux;

    // Constructor
    Poisson(
      double dt,
      double tau_refrac,
      double init_rate,
      uint8_t max_consec,
      double init_time,
      bool is_independent
    );

    // Methods
    static double generate_random_func();

    void step(uint32_t sample);

    void set_target_rate(double target_rate);

    void reset_consec();
};

#endif
